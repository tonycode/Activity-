/**
 * Created by cacalot123 on 2016/5/16.
 */
(function () {
    //$("#rotateBg").css("backgroundImage",url(ratateData.ratateBg));


    var address_info = '<div id="consigneeInfoContainer">' +
        '        <div class="consigneeInfoContainer">' +
        '        <div id="consigneeInfoName" class="consigneeInfoRow">' +
        '        <div class="consigneeInfoColumnLeft"><span class="asterisk">*</span><span class="consigneeInfoDesc">收货人：</span></div>' +
        '    <div class="consigneeInfoColumnRight">' +
        '        <input type="text" value="" class="person"/><span class="consigneeInfoErrorSpan"><i class="ECar_icon_warn consigneeInfoIcon"></i><span class="consigneeInfoWarning">收货人不能为空</span></span>' +
        '        </div>' +
        '        </div>' +
        '        <div id="consigneeInfoCity" class="consigneeInfoRow">' +
        '        <div class="consigneeInfoColumnLeft"><span class="asterisk">*</span><span class="consigneeInfoDesc">所在地区：</span></div>' +
        '    <div class="consigneeInfoColumnRight">' +
        '<div class="select_container">' +
        '    <div class="select_result"><span>' +
        '        <label>上海长宁区城区</label><i class="select_icon"></i></span></div>' +
        '    <div class="select_list">' +
        '        <dl>' +
        '            <dt class="select_nav">' +
        '            <ul>' +
        '                <li data-type="province" class="current">上海</li>' +
        '                <li data-type="city">长宁区</li>' +
        '                <li data-type="region">城区</li>' +
        '            </ul>' +
        '            </dt>' +
        '            <dd class="select_item">' +
        '                <ul data-item-type="province" class="active">' +
        '                    <li><a href="javascript:;">城区</a></li>' +
        '                </ul>' +
        '                <ul data-item-type="city"></ul>' +
        '                <ul data-item-type="region"></ul>' +
        '            </dd>' +
        '        </dl>' +
        '    </div>' +
        '</div>' +
        '        <span class="consigneeInfoErrorSpan"><i class="ECar_icon_warn consigneeInfoIcon"></i><span class="consigneeInfoWarning"></span></span>' +
        '        </div>' +
        '        </div>' +
        '        <div id="consigneeInfoAddress" class="consigneeInfoRow">' +
        '        <div class="consigneeInfoColumnLeft"><span class="asterisk">*</span><span class="consigneeInfoDesc">详细地址：</span></div>' +
        '    <div class="consigneeInfoColumnRight">' +
        '        <input type="text" class="address"/><span class="consigneeInfoErrorSpan"><i class="ECar_icon_warn consigneeInfoIcon"></i><span class="consigneeInfoWarning">地址不能为空</span></span>' +
        '        </div>' +
        '        </div>' +
        '        <div id="consigneeInfoPhone" class="consigneeInfoRow">' +
        '        <div class="consigneeInfoColumnLeft"><span class="asterisk">*</span><span class="consigneeInfoDesc">手机号码：</span></div>' +
        '    <div class="consigneeInfoColumnRight">' +
        '        <input type="text" class="phoneNo"/><span class="consigneeInfoErrorSpan"><i class="ECar_icon_warn consigneeInfoIcon"></i><span class="consigneeInfoWarning">手机号码格式不正确</span></span>' +
        '        </div>' +
        '        </div>' +
        '        <div class="ECar_modal_button">' +
        '        <button class="consigneeInfoBtn" id="consigneeInfoBtn">保存收货人信息</button>' +
        '        </div>' +
        '     </div>' +
        ' </div>';


    ECar.modal.otherTemplate({
        width: 525,
        height: 300,
        title: "通知",
        maskAlpha: "1",
        template: address_info
    });
    var formValidation =
    {
        "box": "userAddressList",
        "list": [
            {"id": "consigneeInfoName", input: "", "rule": "isEmpty"},
            {"id": "consigneeInfoCity", input: "", "rule": "isEmpty"},
            {"id": "consigneeInfoAddress", input: "", "rule": "isEmpty"},
            {"id": "consigneeInfoPhone", input: "", "rule": "isMobile"}
        ],
        "submit": "#consigneeInfoBtn"
    }
    payForm(formValidation);


    /*    define(["./jQueryRotate.2.2.js", "./jquery.easing.min.js"], function () {
     $("#lotteryBtn").rotate({
     bind: {
     click: function () {
     prizeReault(ratateData)
     }
     }
     })*/

//验证


    function payForm(form) {
        for (var i = 0; i < form.list.length; i++) {
            var id = form.list[i].id,
                rule = form.list[i].rule;
            if (rule == "isEmpty") {
                $("#" + id + " input").on("keyup blur", function () {

                    var $self = $(this),
                        $errorTips = $self.next(".consigneeInfoErrorSpan");
                    if (ECar.reg.isEmpty($self.val())) {
                        $errorTips.show();
                        $(this).attr("data-flag","false")
                    } else {
                        $errorTips.hide();
                        $(this).attr("data-flag","ok")
                    }
                })
            } else if (rule == "isMobile") {
                $("#" + id + " input").on("keyup blur", function () {
                    var $self = $(this),
                        $errorTips = $self.next(".consigneeInfoErrorSpan");
                    if (!ECar.reg.isMobile($self.val())) {
                        $errorTips.show();
                        $(this).attr("data-flag","false")
                    } else {
                        $errorTips.hide();
                        $(this).attr("data-flag","ok")
                    }
                })
            }
        }

        $(form.submit).on("click",function(){
            var flag = true;
            for(var i = 0; i < form.list.length; i++) {
                var list = form.list[i],
                    $input = $("#" + list.id).find("input");
                $input.blur();
                if($input.length != 0){
                    if($input.attr("data-flag") != "ok"){
                        flag = false;
                        break;
                    }
                }
            }
            if(flag){//success
                var addressInfo = {};
                $(".ECar_modal_mask").hide();
            }
        })

    }
})();