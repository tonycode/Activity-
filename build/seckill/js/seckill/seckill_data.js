//异步加载器加载js，css
var dynamicLoading = {
    css: function(path){
		if(!path || path.length === 0){
			throw new Error('argument "path" is required !');
		}
		var head = document.getElementsByTagName('head')[0];
        var link = document.createElement('link');
        link.href = path;
        link.rel = 'stylesheet';
        link.type = 'text/css';
        head.appendChild(link);
    },
    js: function(path){
		if(!path || path.length === 0){
			throw new Error('argument "path" is required !');
		}
		var head = document.getElementsByTagName('head')[0];
        var script = document.createElement('script');
        script.src = path;
        script.type = 'text/javascript';
        head.appendChild(script);
    }
}
dynamicLoading.css("css/seckill/common/reset.css");
dynamicLoading.css("dep/seckill/ECar/modal/css/ECar.modal.css");
dynamicLoading.css("css/seckill/Activity_detail_PC/select.css");
dynamicLoading.css("css/seckill/Activity_detail_PC/Activity_detail.css");
dynamicLoading.css("css/seckill/Activity_detail_PC/Address_info.css");

dynamicLoading.js("http://s1.cximg.com/cxlib/assets/js/jQuery/jquery-1.11.3.min.js");
dynamicLoading.js("dep/seckill/Ecar/countDown.js");

window.onload = function(){
	var dataUrl = '',
	address_info;
	$.ajax({
		url: dataUrl,
		dataType: 'json',
		scriptCharset: 'utf-8',
		success: function (data) {
			getCarData(data);
		}
	})

	getCarData();
	function getCarData(data){

		var obj = {
		    "data": {
		        "activityEndtime": "2016-05-31 17:03:35",
		        "activityId": 40,
		        "activityName": "秒杀每天小幸运，iPhone6等你拿",
		        "activityStarttime": "2016-05-17 17:03:35",
		        "awardAmount": 8,
		        "cashPrice": 0.11,
		        "pointPrice": 99,
		        "marketPrice": 1000,
		        "awardName": "2014款1.5L手动朝东超值版 (7座)",
		        "mainImage" : "img/seckill/car.jpg"
		    },
		    "isSuccess": true,
		    "message": "成功"
		}
		// var obj = data;
		var postStr = '<div class="detail_head">'+
		'      <div class="title clearfix"><span class="title_txt">'+ obj.data.activityName +'</span></div>'+
		'      <div class="goodport clearfix">'+
		'        <div class="good_img"><img src="'+ obj.data.mainImage +'"></div>'+
		'        <div class="good_detail">'+
		'          <div class="good_name">'+
		'            <p>'+ obj.data.awardName +'</p>'+
		'          </div>'+
		'          <div class="price_detail">'+
		'            <p class="market">市场价：<span class="market_num">'+ obj.data.marketPrice +'</span>元</p>'+
		'            <p class="sale"><span class="sale_num">'+ obj.data.cashPrice +'</span>元<span class="dot">+</span><span class="point_num">'+ obj.data.pointPrice +'</span>积分<span class="sale_txt">秒杀价</span></p>'+
		'          </div>'+
		'          <div id="countDown" class="countdown"><span class="countdown_txt">倒计时：</span><span class="day time"><span class="timeContainer"><span class="first">0</span><i class="line"></i></span><span class="timeContainer"><span class="last">0</span><i class="line"></i></span></span>天<span class="hour time"><span class="timeContainer"><span class="first">0</span><i class="line"></i></span><span class="timeContainer"><span class="last">0</span><i class="line"></i></span></span>时<span class="minute time"><span class="timeContainer"><span class="first">0</span><i class="line"></i></span><span class="timeContainer"><span class="last">0</span><i class="line"></i></span></span>分<span class="second time"><span class="timeContainer"><span class="first">0</span><i class="line"></i></span><span class="timeContainer"><span class="last">0</span><i class="line"></i></span></span>秒</div>'+
		'          <a href="javasript:;" data-num="'+ obj.data.activityId +'" class="buy_button">即将开始</a>'+
		'          <div class="stock">库存：<span class="stock_num">'+ obj.data.awardAmount +'</span></div>'+
		'        </div>'+
		'      </div>'+
		'    </div>';
		$('body').append(postStr);
	}

	dynamicLoading.js("dep/seckill/ECar/modal/ECar.modal.js");
	dynamicLoading.js("dep/seckill/ECar/ECar.reg-1.0.min.js");
	dynamicLoading.js("js/seckill/moudle/select.js");
	dynamicLoading.js("js/seckill/seckill_detail.js");
}