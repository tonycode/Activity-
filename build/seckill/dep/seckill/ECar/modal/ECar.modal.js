;
(function($, ECar) {
	ECar.modal = (function() {
		//默认配置
		var _settings = {
			maskAlphaColor: "rgba(0, 0, 0, 0.3)",
			title: "提示",
			type: "alert",
			width: 350,
			height: 130,
			content: "", // 内容
			okTxt: "确定", //确定按钮文本信息
			cancelTxt: "取消", //取消按钮文本信息
			template: null,
			message: "",
			hasClose: true, //是否需要右上角关闭按钮
			iconType: "warn",
			okFn: null,
			cancelFn: null,
			closeFn: null,
			saveFn: null,
			loadFn: null
		};

		//modal
		var oModal = (function() {
			var aModalType = ["alert", "confirm", "warn", "otherTemplate"],
				jModal = null;


			//init modal
			function initModal(_settings) {
				//mask
				var jMask = jModal = $("<div>").addClass("ECar_modal_mask"),
					jModalContainer = $("<div>").addClass("ECar_modal"),
					jModalTitle = $("<div>").addClass("ECar_modal_title"),
					jModalBody = $("<div>").addClass("ECar_modal_body");

				//title
				jModalTitle.append($("<h3>").text(_settings.title));
				if(_settings.hasClose) {
					jModalTitle.append($("<i>").addClass("ECar_icon ECar_icon_close"));
				}
				jModalContainer.append(jModalTitle);

				//content
				var sType = _settings.type;
				if (sType === "otherTemplate") {
					jModalBody.append(_settings.template);
				} else if (aModalType.join().indexOf(sType) !== -1) {
					addContent(_settings, jModalBody);
				}

				//message				
				if (sType === "confirm" && _settings.message.length) {
					addMessage(_settings, jModalBody);
				}

				//button
				if (sType === "confirm" || sType === "alert") {
					addButton(_settings, jModalBody, sType);
				}

				jModalContainer.append(jModalBody);
				jMask.append(jModalContainer);
				$(document.body).append(jMask);

				return jMask;
			}
			//update modal
			function updateModal(_settings) {
				var sType = _settings.type,
					jModalBody = $(".ECar_modal_body");

				//update title
				var jModalTitle = $(".ECar_modal_title"),
					jCloseIcon = jModalTitle.find("i");
				jModalTitle.find("h3").text(_settings.title);
				if(_settings.hasClose && jCloseIcon.length === 0) {
					jModalTitle.append($("<i>").addClass("ECar_icon ECar_icon_close"));
				} else if((!_settings.hasClose) && jCloseIcon.length === 1) {
					jCloseIcon.remove();
				}

				if (sType === "otherTemplate") {
					$(".ECar_modal_body").html(_settings.template);
				} else if (aModalType.join().indexOf(sType) !== -1) {

					if($(".ECar_modal_content").length) {
						//update content
						updateContent(_settings);

						//update message
						updateMessage(_settings);
					} else {
						//add content
						addContent(_settings, jModalBody);

						//add message
						addMessage(_settings, jModalBody);
					}
					
					// add or update button
					addButton(_settings, jModalBody, sType);
				}
			}
			//setStyle
			function setStyle(_settings) {
				var oStyle = {
					"width": _settings.width + "px",
					"height": _settings.height + "px",
					"marginLeft": "-" + _settings.width / 2 + "px",
					"marginTop": "-" + _settings.height / 2 + "px"
				}
				jModal.children("div").css(oStyle); //设置弹出层位置
				jModal.css({
					"background-color":_settings.maskAlphaColor
				});
			}
			//add content
			function addContent(_settings, jModalBody) {
				jModalBody.html('');
				var jModalContent = $("<div>").addClass("ECar_modal_content");
				jModalContent.append($("<i>").addClass("ECar_icon").addClass("ECar_icon_" + _settings.iconType));
				jModalContent.append($("<p>").addClass("ECar_modal_msg").html(_settings.content));
				jModalBody.append(jModalContent);
			}
			//update content
			function updateContent(_settings) {
				$(".ECar_modal_content i").removeAttr("class").addClass("ECar_icon").addClass("ECar_icon_" + _settings.iconType);
				$(".ECar_modal_content p").html(_settings.content);
			}
			//add message
			function addMessage(_settings, jModalBody) {
				var jModalMessage = $("<div>").addClass("ECar_modal_message");
				jModalMessage.append("" + _settings.message);
				jModalBody.append(jModalMessage);
			}
			//update message
			function updateMessage(_settings) {
				var jModalMessage = $(".ECar_modal_message");
				if (_settings.type === "confirm" && _settings.message.length) {
					if (jModalMessage.length === 0) {
						jModalMessage = $("<div>").addClass("ECar_modal_message");
						jModalMessage.insertAfter($(".ECar_modal_content"));
					}
					jModalMessage.html("" + _settings.message)
				} else if (jModalMessage.length) {
					jModalMessage.remove();
				}
			}
			//add button
			function addButton(_settings, jModalBody, sType) {
				var jModalButton = $(".ECar_modal_button"),
					jButton = null;
				if (jModalButton.length === 0 && (sType === "alert" || sType === "confirm")) {
					jModalButton = $("<div>").addClass("ECar_modal_button");
					jModalButton.append($("<button>").addClass("red confirm").text(_settings.okTxt));
					if (sType === "confirm") {
						jModalButton.append($("<button>").addClass("cancel").text(_settings.cancelTxt));
					}
					jModalBody.append(jModalButton);
				} else if (sType === "warn") { // "warn"类型弹框无需按钮
					jModalButton.remove();
				} else {
					jModalButton.html("");
					jModalButton.append($("<button>").addClass("red confirm").text(_settings.okTxt));
					if (sType === "confirm") {
						jModalButton.append($("<button>").addClass("cancel").text(_settings.cancelTxt));
					}
				}
			}

			return {
				flag: true,
				get: function(_settings) {
					return jModal || (this.flag = false, initModal(_settings));
				},
				update: function(_settings) {
					updateModal(_settings);
				},
				show: function(fn) {
					this.get().show();
					fn && setTimeout(fn, 0);
				},
				hide: function(fn) {
					this.get().hide();
					fn && setTimeout(fn, 0);
				},
				setStyle: function(_settings) {
					setStyle(_settings);
				}
			}
		})();

		function addEvent(obj, fn) {
			$(obj).on("click", function(e) {
				oModal.hide(fn);
				e.preventDefault();
				e.stopPropagation();
			})
		}

		function show(option, type) {
			var settings = $.extend({}, _settings, option);
			settings.type = type;

			switch (settings.iconType) {
				case "warn":
					break;
				case "error":
					break;
				case "success":
					break;
				default:
					settings.iconType = "warn";
			}
			oModal.flag ? oModal.get(settings) : oModal.update(settings);

			oModal.setStyle(settings);
			oModal.show(function() {
				settings.loadFn && setTimeout(settings.loadFn, 0);
				addEvent($(".ECar_icon_close"), settings.closeFn);
				addEvent($(".ECar_modal .confirm"), settings.okFn);
				addEvent($(".ECar_modal .cancel"), settings.cancelFn);
				addEvent($(".ECar_modal .save"), settings.saveFn);
			});
		}

		function hide(fn){
			oModal.hide(fn);
		}

		return {
			"alert": function() {
				show(arguments[0], "alert");
			},
			"confirm": function() {
				show(arguments[0], "confirm");
			},
			"warn": function() {
				show(arguments[0], "warn");
			},
			"otherTemplate": function() {
				show(arguments[0], "otherTemplate");
			},
			"hide": function(){
				hide(arguments[0]);
			}
		}
	})();

})(jQuery, window.ECar || (window.ECar = {}));