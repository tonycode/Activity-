;(function($){
	"use strict";

	var storage = (function(){
		var dataDom = null,
			hostName = location.hostName ? location.hostName : "static",
			isSessionStorage = window.sessionStorage ? true : false;

		function initDataDom(){
			if(dataDom) return dataDom;

			try{
				dataDom = document.createElement('input');
				dataDom.type = 'hidden';
				dataDom.style.display = "none";
				dataDom.addBehavior('#default#userData');
				body.appendChild(this.dataDom);
				var exDate = new Date();
				exDate = exDate.getDate()+30;
				dataDom.expires = exDate.toUTCString();//设定过期时间
			} catch(e){
				dataDom = null;
			}

			return dataDom;
		}

		function setItem(key, value){
			if(isSessionStorage) {
				widnow.sessionStorage.setItem(key, value);
			} else if(initDataDom()) {
				dataDom.load(hostName);
				dataDom.setAttribute(key, value);
				dataDom.save(hostName);
			}
		}

		function getItem(key) {
			if(isSessionStorage) {
				return window.sessionStorage.getItem(key);
			} else if(initDataDom()) {
				dataDom.load(hostName);
				return dataDom.getAttribute(key);
			}
		}

		return {
			setItem: function(){
				setItem(arguments);
			},
			getItem: function(){
				getItem(arguments);
			}
		}
	})();

	$(".select_nav li").on("click", function(e){
		var sType = $(this).data("type");
		// 切换"选中"状态

		toggleDisplay(regionMap(sType));
	});

	$(".select_item").on("click", function(e){
		var oSelf = e.target,
			sNodeName = oSelf.nodeName.toLowerCase(),
			sText = $(oSelf).text(),
			jParent = $(oSelf).parent(),
			sType = jParent.parent("ul").data("item-type");

		if(sNodeName !== "a") return;

		//更新导航显示区域
		updateNavTxt(sType, sText);

		// 切换标签显示状态
		if(jParent.hasClass("current")) return;
		toggleState(jParent, "current");

		// 切换导航显示状态
		var nType = regionMap(sType);
		if(nType === 1) {
			updateNavTxt("city", "请选择");
			updateNavTxt("region", "请选择");
			$(".select_nav").find("li[data-type='region']").hide();
		} else if(nType === 2) {
			updateNavTxt("region", "请选择");
			$(".select_nav").find("li[data-type='region']").show();
		}

		nType = nType > 2 ? 0 : (nType + 1);
		toggleDisplay(nType);
	});

	function regionMap(type) {
		var oMapping = {
				"province": 1,
				"city": 2,
				"region": 3
			};

		return oMapping[type] ? oMapping[type] : 0;
	}

	function toggleDisplay(type){
		var sNav = "",
			sItem = "";
		switch(type) {
			case 1:
				sNav = "data-type='province'";
				sItem = "data-item-type='province'";
				break;
			case 2:
				sNav = "data-type='city'";
				sItem = "data-item-type='city'";
				break;
			case 3:
				sNav = "data-type='region'";
				sItem = "data-item-type='region'";
				break;
		}

		if(sNav && sItem) {
			//显示对应的导航
			var jNav = $(".select_nav");
			toggleState(jNav.find("li[" + sNav + "]"), "current");
			//显示对应的区域
			$(".select_item").find("ul[" + sItem + "]").show().siblings().hide();
		}
	}

	function createItem(type, data, name) {
		var jSelector = $(".select_item").find("ul[data-item-type='" + type + "']");

		if(jSelector.length) {
			var jLi = null,
				sRegionName = "",
				jA = null;
			for(var i = 0; i < data.length; i++) {
				sRegionName = data[i].regionName;

				jLi = $("<li>");
				jA = $("<a href='javascript:;'>").attr("title", sRegionName).text(sRegionName);
				
				if(sRegionName === name) {
					jLi.addClass("current");
				}
				
				jLi.append(jA);
				jSelector.append(jLi);
			}
		}
	}

	function getRegionData(code){
		var oResult = storage.getItem(code);

		if(oResult) return oResult;

		$.ajax({
			url: "10.47.17.165/base/getProviceAddr",
			type: "post",
			async: false,
			dataType: "json",
			success: function(data){
				console.dir(data);
				oResult = JSON.parse(data);
				storage.setItem(code, oResult);
			},
			fail: function(){

			}
		})
	}

	// 更新导航显示区域
	function updateNavTxt(type, text) {
		var jSelector = $(".select_nav").find("li[data-type='" + type + "']");

		if(jSelector.length === 1) {
			jSelector.eq(0).text(text);
		}
	}

	// 切换元素状态
	function toggleState(o, className) {
		if(o instanceof $ && className.length) {
			o.addClass(className).siblings().removeClass(className);
		}
	}
})(jQuery);