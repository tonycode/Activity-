/**
 * Created by cacalot123 on 2016/5/16.
 */
(function() {
    //$("#rotateBg").css("backgroundImage",url(ratateData.ratateBg));


    var address_info = '<div id="consigneeInfoContainer">' +
        '        <div class="consigneeInfoContainer">' +
        '        <div id="consigneeInfoName" class="consigneeInfoRow">' +
        '        <div class="consigneeInfoColumnLeft"><span class="asterisk">*</span><span class="consigneeInfoDesc">收货人：</span></div>' +
        '    <div class="consigneeInfoColumnRight">' +
        '        <input type="text" value="" class="person"/><span class="select">选择地址</span><span class="consigneeInfoErrorSpan"><i class="ECar_icon_warn consigneeInfoIcon"></i><span class="consigneeInfoWarning">收货人不能为空</span></span>' +
        '        </div>' +
        '        </div>' +
        '        <div id="consigneeInfoCity" class="consigneeInfoRow">' +
        '        <div class="consigneeInfoColumnLeft"><span class="asterisk">*</span><span class="consigneeInfoDesc">所在地区：</span></div>' +
        '    <div class="consigneeInfoColumnRight">' +
        '<div class="select_container">' +
        '    <div class="select_result"><span>' +
        '        <label>上海长宁区城区</label><i class="select_icon"></i></span></div>' +
        '    <div class="select_list">' +
        '        <dl>' +
        '            <dt class="select_nav">' +
        '            <ul>' +
        '                <li data-type="province" class="current">上海</li>' +
        '                <li data-type="city">长宁区</li>' +
        '                <li data-type="region">城区</li>' +
        '            </ul>' +
        '            </dt>' +
        '            <dd class="select_item">' +
        '                <ul data-item-type="province" class="active">' +
        '                    <li><a href="javascript:;">城区</a></li>' +
        '                </ul>' +
        '                <ul data-item-type="city"></ul>' +
        '                <ul data-item-type="region"></ul>' +
        '            </dd>' +
        '        </dl>' +
        '    </div>' +
        '</div>' +
        '        <span class="consigneeInfoErrorSpan"><i class="ECar_icon_warn consigneeInfoIcon"></i><span class="consigneeInfoWarning"></span></span>' +
        '        </div>' +
        '        </div>' +
        '        <div id="consigneeInfoAddress" class="consigneeInfoRow">' +
        '        <div class="consigneeInfoColumnLeft"><span class="asterisk">*</span><span class="consigneeInfoDesc">详细地址：</span></div>' +
        '    <div class="consigneeInfoColumnRight">' +
        '        <input type="text" class="address"/><span class="consigneeInfoErrorSpan"><i class="ECar_icon_warn consigneeInfoIcon"></i><span class="consigneeInfoWarning">地址不能为空</span></span>' +
        '        </div>' +
        '        </div>' +
        '        <div id="consigneeInfoPhone" class="consigneeInfoRow">' +
        '        <div class="consigneeInfoColumnLeft"><span class="asterisk">*</span><span class="consigneeInfoDesc">手机号码：</span></div>' +
        '    <div class="consigneeInfoColumnRight">' +
        '        <input type="text" class="phoneNo"/><span class="consigneeInfoErrorSpan"><i class="ECar_icon_warn consigneeInfoIcon"></i><span class="consigneeInfoWarning">手机号码格式不正确</span></span>' +
        '        </div>' +
        '        </div>' +
        '        <div class="ECar_modal_button">' +
        '        <button class="consigneeInfoBtn" id="consigneeInfoBtn">提交</button>' +
        '        <button class="backBtn" id="backBtn">返回</button>' +
        '        </div>' +
        '     </div>' +
        ' </div>';

    var address_list_template = '' +
        '<div class="address_select">' +
        '<div class="address_list">' +
        '<table>' +
        '<thead>' +
        '<tr>' +
        '<th width="10%">收货人</th>' +
        '<th width="20%">所在地区</th>' +
        '<th width="35%">街道地址</th>' +
        '<th width="10%">邮编</th>' +
        '<th width="15%">手机</th>' +
        '<th width="10%">操作</th>' +
        '</tr>' +
        '</thead>' +
        '<tbody>' +
        '<tr>' +
        '<td colspan="6" class="ta-c pd_l_0">' +
        '数据加载中<span class="address_loading"></span>' +
        '</td>' +
        '</tr>' +
        '</tbody>' +
        '</table>' +
        '</div>' +
        '<div class="btn_container">' +
        '<button class="button close_btn">关闭</button>' +
        '</div>' +
        '</div>';

    // 显示收货信息弹出框
    function showModal(callback) {
        ECar.modal.otherTemplate({
            width: 840,
            height: 450,
            title: "地址选择",
            template: address_info,
            loadFn: callback,
            closeFn: null
        });
    }

    showModal(loadFn);

    // 弹出框加载事件
    function loadFn() {
        $(".select").on("click", function(e) {
            var result = null;
            storeAddress();
            $.ajax({
                url: "url"
            }).done(function(data) {
                setTimeout(function() {
                    result = createAddress(data);
                    $(".address_list tbody").html("").append(result);

                    // 选择地址
                    $(".select_btn").on("click", function(e) {
                        var $td = $(this).parents("tr").find("td");
                        store.set("user", {
                            "name": $td.eq(0).text(),
                            "detail": $td.eq(2).text(),
                            "phone": $td.eq(4).text()
                        });

                        showModal(function() {
                            loadFn();
                            setAddress();
                        });
                    });
                }, 1000);
            }).fail(function() {
                result = {};
                setTimeout(function() {
                    result = $(createAddress(result));
                    $(".address_list tbody").html("").append(result);

                    // 选择地址
                    $(".select_btn").on("click", function(e) {
                        var $td = $(this).parents("tr").find("td");
                        store.set("user", {
                            "name": $td.eq(0).text(),
                            "detail": $td.eq(2).text(),
                            "phone": $td.eq(4).text()
                        });

                        showModal(function() {
                            loadFn();
                            setAddress();
                        });
                    });
                }, 1000);
            }).always(function() {
                ECar.modal.otherTemplate({
                    width: 840,
                    height: 450,
                    title: "地址选择",
                    needHide: false,
                    template: address_list_template,
                    loadFn: function() {
                        $(".close_btn").on("click", function() {
                            showModal(function() {
                                loadFn();
                                setAddress();
                            });
                        });
                    },
                    closeFn: function() {
                        showModal(function() {
                            loadFn();
                            setAddress();
                        });
                    }
                });
            });
        });

        $(".backBtn").on("click", function() {
            ECar.modal.hide();
        });
    }

    // 遍历地址信息
    function createAddress(data) {
        var documentFragment = document.createDocumentFragment(),
            $tr = null,
            $td = null;
        if (data) { // 存在地址信息
            for (var i = 0; i < 3; i++) {
                $tr = $("<tr>");

                $tr.append($("<td>").text("小李" + i));
                $tr.append($("<td>").text("新疆地区 吐鲁番市"));
                $tr.append($("<td>").addClass("pd_r_30").text("阿吉阿吉阿吉阿吉阿吉阿吉阿吉阿吉阿吉阿吉阿吉阿吉阿吉阿吉"));
                $tr.append($("<td>").text("200333"));
                $tr.append($("<td>").text("13917874367"));
                $tr.append($("<td>").append($("<button>").addClass("button select_btn").text("选择")));

                documentFragment.appendChild($tr[0]);
                $tr = $td = null;
            }
        } else { // 无数据
            $tr = $("<tr>");
            $tr.append($("<td>").addClass("ta-c pd_l_0").attr("colspan", "6").text("暂无数据"));
            documentFragment.appendChild($tr[0]);
            $tr = null;
        }

        return documentFragment;
    }

    // 存储用户数据
    function storeAddress() {
        store.set("user", {
            "name": $(".person").val(),
            "detail": $(".address").val(),
            "phone": $(".phoneNo").val()
        });
    }

    // 数据回填
    function setAddress() {
        var user = store.get("user");
        if (user) {
            $(".person").val(user.name);
            $(".address").val(user.detail);
            $(".phoneNo").val(user.phone);
        }
        store.remove("user");
    }

    var formValidation = {
        "box": "userAddressList",
        "list": [{
            "id": "consigneeInfoName",
            input: "",
            "rule": "isEmpty"
        }, {
            "id": "consigneeInfoCity",
            input: "",
            "rule": "isEmpty"
        }, {
            "id": "consigneeInfoAddress",
            input: "",
            "rule": "isEmpty"
        }, {
            "id": "consigneeInfoPhone",
            input: "",
            "rule": "isMobile"
        }],
        "submit": "#consigneeInfoBtn"
    }
    payForm(formValidation);


    /*    define(["./jQueryRotate.2.2.js", "./jquery.easing.min.js"], function () {
     $("#lotteryBtn").rotate({
     bind: {
     click: function () {
     prizeReault(ratateData)
     }
     }
     })*/

    //验证


    function payForm(form) {
        for (var i = 0; i < form.list.length; i++) {
            var id = form.list[i].id,
                rule = form.list[i].rule;
            if (rule == "isEmpty") {
                $("#" + id + " input").on("keyup blur", function() {

                    var $self = $(this),
                        $errorTips = $self.next(".consigneeInfoErrorSpan");
                    if (ECar.reg.isEmpty($self.val())) {
                        $errorTips.show();
                        $(this).attr("data-flag", "false")
                    } else {
                        $errorTips.hide();
                        $(this).attr("data-flag", "ok")
                    }
                })
            } else if (rule == "isMobile") {
                $("#" + id + " input").on("keyup blur", function() {
                    var $self = $(this),
                        $errorTips = $self.next(".consigneeInfoErrorSpan");
                    if (!ECar.reg.isMobile($self.val())) {
                        $errorTips.show();
                        $(this).attr("data-flag", "false")
                    } else {
                        $errorTips.hide();
                        $(this).attr("data-flag", "ok")
                    }
                })
            }
        }

        $(form.submit).on("click", function() {
            var flag = true;
            for (var i = 0; i < form.list.length; i++) {
                var list = form.list[i],
                    $input = $("#" + list.id).find("input");
                $input.blur();
                if ($input.length != 0) {
                    if ($input.attr("data-flag") != "ok") {
                        flag = false;
                        break;
                    }
                }
            }
            if (flag) { //success
                var addressInfo = {};
                $(".ECar_modal_mask").hide();
            }
        })

    }
})();