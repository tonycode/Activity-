var ECar=ECar || {};
ECar.countDown=ECar.countDown||{};

ECar.countDown=(function(){
	var interval=null,
		defaults={
			id:'countDown',				//倒计时显示对象ID
			step:1000,					//倒计时间隔时间
			start:new Date(),			//倒计时开始时间
			stop:null,					//倒计时结束时间
			duration:0,					//倒计时持续时间
			calculateFn:null,			//在间隔时间内重复运行函数
			callbackFn:null,			//回调函数
			errorFn:null				//参数错误函数
		};		
		//stop或duration属性需要从外部传入，全部传入时以duration为准。stop：倒计时结束时间；duration：倒计时持续时间。

	function initilize(options){		//倒计时对象初始化
		//输入参数合法性判断
		isDate(options['start']) || delete options['start'];
		isDate(options['stop']) || delete options['stop'];

		//输入参数与默认值合并
		for(var opt in defaults){
			if(options[opt]===undefined){
				options[opt]=defaults[opt];
			}
		}
		//检查参数并开始倒计时
		options['dom']=document.getElementById(options['id']);
		if(checkOptions(options)){
			startCountDown(options);
		}
		else{
			options['errorFn'] && options['errorFn'].call(options['dom'],options);
		}
	}
	//判断参数start和stop是否是日期
	function isDate(date){
		return Object.prototype.toString.call(date)==='[object Date]';
	}
	//检查输入参数stop和duration
	function checkOptions(options){
		if(options['stop']===null && options['duration']===0){
			return false;
		}		
		else if(options['duration']){
			options['stop']=new Date(options['start'].getTime()+options['duration']);
		}
		else{
			options['duration']=options['stop']-options['start'];
		}		
		if(options['duration']>0){
			return true;
		}
		else{
			return false;
		}
	}
	//计算倒计时显示的数字
	function calculate(options,timeRest){
		var secondTotal=Math.floor(timeRest/1000),
			day=Math.floor(secondTotal/86400),
			hour=Math.floor((secondTotal-day*86400)/3600),
			minute=Math.floor((secondTotal-day*86400-hour*3600)/60),
			second=secondTotal%60,
			obj={
				day:day,
				hour:hour,
				minute:minute,
				second:second,
				secondTotal:secondTotal
			};
		
		options['calculateFn'] && options['calculateFn'].call(options['dom'],obj);
	}
	//开始倒计时
	function startCountDown(options){
		var timeRest=options['duration'],
			step=options.step;

		calculate(options,timeRest);
		interval=setInterval(function(){
			timeRest-=step;
			if(timeRest>=0){
				calculate(options,timeRest);
			}
			else{
				clearInterval(interval);
				options['callbackFn'] && options['callbackFn'].call(options['dom'],options);
			}			
		},step);
	}
	//停止倒计时
	function stopCountDown(){
		clearInterval(interval);
	}

	return{
		start:function(options){
			initilize(options);
		},
		stop:function(){
			stopCountDown();
		}
	}

})();